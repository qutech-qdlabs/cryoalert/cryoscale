# CryoScale

The CryoScale is a modified scale to measure weight and for this weight to be read by a CryoAgent. This repository contains the code run on the Arduino within the scale.

## Retrieving data

The Arduino is connected using a serial connection (USB) with a baud rate of 115200.

### Calibrate to zero

To calibrate the scale to zero, remove all objects from the scale and then send the `C0` command to Arduino over the serial connection. The measured raw value is stored on the Arduino's EEPROM which will be retrieved during initialization.

### Calibrate with known weight

To calibrate the scale to a known weight (used to determine how much the raw measurements change with weight, assuming a linear relation), put an object with a known weight on the scale and send the `CW<known_weight_value>` command to the Arduino. Note that `<known_weighe_value>` need to be provided by the user, so the command should look like `CW5.1` for a known weight of 5.1 kg's (or any other weight unit). All floats are acceptable values (except `NaN`) and the unit used for the weight will be units in which the scale will return its values. The Arduino will compute a scaling factor which is then stored on the Arduino's EEPROM and which will be retrieved during initialization.

### Retrieving weight measurement

To retrieve a weight measurement, simply send the `W` command to the Arduino. The Arduino will return the measured weight using the units used during calibration.
