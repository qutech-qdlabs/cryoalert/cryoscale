#include <EEPROM.h>
#include "HX711.h"

HX711 scale;

uint8_t dataPin = 3;
uint8_t clockPin = 2;

int eeAddressScaleOffset = 0;
int eeAddressScaleScale = sizeof(long);


void write_scale_offset_to_eeprom(long offset){
  EEPROM.put(eeAddressScaleOffset, offset);
}

long get_scale_offset_from_eeprom(){
  long offset;
  EEPROM.get(eeAddressScaleOffset, offset);
  return offset;
}

void write_scale_scale_to_eeprom(float scale){
  EEPROM.put(eeAddressScaleScale, scale);
}

float get_scale_scale_from_eeprom(){
  float scale;
  EEPROM.get(eeAddressScaleScale, scale);
  return scale;
}


void setup()
{
  Serial.begin(115200);
  while (!Serial){
    ; // Wait for serial port to connect
  }

  // Set the scale pins
  scale.begin(dataPin, clockPin);

  // Set the data to the scale object
  scale.set_scale(get_scale_scale_from_eeprom()); 
  scale.set_offset(get_scale_offset_from_eeprom());
}


void loop()
{
  // Wait untill something is available in the buffer
  while (Serial.available() == 0) {}

  // Define empty response string
  String resp = "";

  // Read string from buffer and trim it 
  String data = Serial.readString();
  data.trim();

  if (scale.is_ready()) 
  {
    if (data.startsWith("C0"))
    {
      // Zeroing the scale
      // Read offset from average readout
      long offset = scale.read_average(20);

      // Write the offset to the EEPROM
      write_scale_offset_to_eeprom(offset);

      // Set the internal offset of the scale object
      scale.set_offset(offset);

      // Print some response to what has been done
      Serial.println(String("Written offset value` ") + offset + String("` to EEPROM"));
    } else if (data.startsWith("CW"))
    {
      // Calibrate the scale factor of the scale with a known weight, which is the numeric value in KG's 
      // After the "CW" string
      String string_value = data.substring(2);
      float known_weight = string_value.toFloat();

      // For some reason, the "set_scale" method requires the inverse of the actual scale, so compute that directly
      float invScale = (scale.read_average(20) - scale.get_offset()) / (known_weight);

      // Write the (inverse) linear scale to the EEPROM
      write_scale_scale_to_eeprom(invScale);

      // Set the internal (linear) scale of the scale object
      scale.set_scale(invScale);

      // Print some response to what has been done
      Serial.println(String("Written (inverse) scale value`") + String(invScale,3) + String("` to EEPROM"));
    } else if (data.startsWith("W"))
    {
      // Return just the value
      Serial.println(scale.get_units(5));
    }
  }
}